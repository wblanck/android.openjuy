package com.kerbun.openjuy.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by wblanck
 */
public class Ordenanza {

    @SerializedName("Ordenanzaanio")
    private String año;

    @SerializedName("Ordenanzanumero")
    private String numero;

    @SerializedName("Ordenanzafecha")
    private String fecha;

    @SerializedName("Ordenanzasintesis")
    private String sintesis;

    @SerializedName("Ordenanzatema")
    private String tema;

    public String getAño() {
        return año;
    }

    public String getNumero() {
        return numero;
    }

    public String getFecha() {
        return fecha;
    }

    public String getSintesis() {
        return sintesis;
    }

    public String getTema() {
        return tema;
    }

    public String getDetalle() {
        return String.format("%s - %s", año, sintesis);
    }

    public String getPdf() {
        return "http://www.rosario.gov.ar/normativa/verArchivo?tipo=pdf&id=103362";
    }
}
