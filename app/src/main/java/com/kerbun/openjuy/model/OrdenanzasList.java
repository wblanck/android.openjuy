package com.kerbun.openjuy.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by wblanck
 */

public class OrdenanzasList {

    @SerializedName("items")
    private Ordenanza[] items;

    @SerializedName("count")
    private Integer count;

    @SerializedName("hasMore")
    private Boolean hasMore;

    @SerializedName("limit")
    private Integer limit;

    @SerializedName("offset")
    private Integer offset;

    public Ordenanza[] getItems() {
        return items;
    }

    public Integer getCount() {
        return count;
    }

    public Boolean getHasMore() {
        return hasMore;
    }

    public Integer getLimit() {
        return limit;
    }

    public Integer getOffset() {
        return offset;
    }
}
