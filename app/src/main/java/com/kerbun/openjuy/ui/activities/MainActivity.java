package com.kerbun.openjuy.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.kerbun.openjuy.R;
import com.kerbun.openjuy.model.Ordenanza;
import com.kerbun.openjuy.model.OrdenanzasList;
import com.kerbun.openjuy.ui.views.OrdenanzaView;

import java.io.InputStream;

/**
 * Created by wblanck
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadOrdenanzas();
    }


    private void loadOrdenanzas() {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://10.10.20.136:8080/jujuy-libre/ordenanza/getOrdenanza";

        // Request a string response from the provided URL.
        ResponseListener responseListener = new ResponseListener();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, responseListener, responseListener);
        queue.add(stringRequest);
    }

    class ResponseListener implements Response.Listener<String>, Response.ErrorListener {

        @Override
        public void onResponse(String response) {
            try {
                OrdenanzasList ordenanzasList = new Gson().fromJson(response, OrdenanzasList.class);
                renderOrdenanzas(ordenanzasList.getItems());
            } catch (Exception e) {
                onErrorResponse(null);
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            try {
                InputStream inputStream = getResources().openRawResource(R.raw.ordenanzas);
                byte[] b = new byte[inputStream.available()];
                inputStream.read(b);
                String json = new String(b);
                OrdenanzasList ordenanzasList = new Gson().fromJson(json, OrdenanzasList.class);
                Log.i(TAG, "getOrdenanzas: " + ordenanzasList.getItems().length);
                renderOrdenanzas(ordenanzasList.getItems());
            } catch (Exception exception) {
                //TODO handle this
            }
        }
    }

    private void renderOrdenanzas(Ordenanza[] ordenanzas) {

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.main_ordenanzas_list);
        linearLayout.removeAllViews();

        for (Ordenanza ordenanza : ordenanzas) {
            Log.i(TAG, "onCreate: adding ordenanza");
            OrdenanzaView ordenanzaView = new OrdenanzaView(MainActivity.this, ordenanza);
            linearLayout.addView(ordenanzaView);
        }
    }
}
