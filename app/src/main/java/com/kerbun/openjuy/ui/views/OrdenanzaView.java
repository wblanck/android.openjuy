package com.kerbun.openjuy.ui.views;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kerbun.openjuy.R;
import com.kerbun.openjuy.model.Ordenanza;

import java.net.URI;
import java.net.URL;

/**
 * Created by wblanck
 */
public class OrdenanzaView extends LinearLayout {

    private Ordenanza ordenanza;

    public OrdenanzaView(Context context, Ordenanza ordenanza) {
        super(context);
        this.ordenanza = ordenanza;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.ordenanza_view, this, true);
        init();
    }


    private void init() {
        TextView tema = (TextView) findViewById(R.id.ordenanza_view_detail_tema);
        TextView detalle = (TextView) findViewById(R.id.ordenanza_view_detail_detalle);
        ImageView imageView = (ImageView) findViewById(R.id.ordenanza_view_pdf);

        tema.setText(ordenanza.getTema());
        detalle.setText(ordenanza.getDetalle());

        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = ordenanza.getPdf();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setDataAndType(Uri.parse(url), "text/html");
                getContext().startActivity(browserIntent);
            }
        });
    }
}
